package controlador;

import model.Logica;
import model.conexion.ConfConection;
import model.vo.Cool;
import model.vo.Database;
import model.vo.DatabaseList;
import model.vo.Table;
import vista.ColumnOptions;
import vista.ConectionOptions;
import vista.FileOptions;
import vista.Results;
import vista.VentanaPrincipal;

/**
 *
 * @author lukas
 * 
 * Clase encargada de establecer las relaciones entre los diferentes compopnentes
 * de la aplicacion
 * @version 1.1.16
 */
public class Coordinador {

    private Logica logica;
    private VentanaPrincipal ventanaPrincipal;
    private ConectionOptions conectionOptions;
    private ColumnOptions columnOptions;
    private ConfConection cf;
    private FileOptions fileOptions;
    private Results results;

    /**
     *
     * @return Results Ventana de resultados
     */
    public Results getResults() {
        return results;
    }

    /**
     * 
     * @param results type: Results
     * 
     * Define la ventana de Resultados
     */
    public void setResults(Results results) {
        this.results = results;
    }

    /**
     *
     * @return Debuelve ConfConection
     * 
     * La configuracion de la conexion a la base de datos
     */
    public ConfConection getCf() {
        return cf;
    }

    /**
     *
     * @param cf type: ConfConection
     * 
     * La configuracionde la base de datos
     */
    public void setCf(ConfConection cf) {
        this.cf = cf;
    }

    /**
     *
     * @return ColumnOptions 
     * Devuelve la ventana de la configuracion de la columna
     */
    public ColumnOptions getColumnOptions() {
        return columnOptions;
    }

    /**
     *
     * @param columnOptions type ColumnOptions
     * 
     * Establece la vantana de configuracion de la columna
     */
    public void setColumnOptions(ColumnOptions columnOptions) {
        this.columnOptions = columnOptions;
    }

    /**
     *
     * @return VentanaPrincipal 
     * 
     * Devuelve la ventana principal
     */
    public VentanaPrincipal getVentanaPrincipal() {
        return ventanaPrincipal;
    }

    /**
     *
     * @param miVentanaPrincipal type: VentanaPrincipal
     * 
     * Establece la ventana principal del programa
     */
    public void setVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
        this.ventanaPrincipal = miVentanaPrincipal;
    }

    /**
     *
     * @return Logica
     * 
     * devuelve la logica del programa.
     */
    public Logica getLogica() {
        return logica;
    }

    /**
     *
     * @param miLogica type Logica
     * 
     * Establece la logca del programa
     */
    public void setLogica(Logica miLogica) {
        this.logica = miLogica;
    }

    /**
     *
     * @return ConectionOptions
     * 
     * devuelve la ventana de configracion de la conexion
     */
    public ConectionOptions getConectionOptions() {
        return conectionOptions;
    }

    /**
     *
     * @param conectionOptions type ConectionOptions
     * 
     * Establece la ventana de la configuracion de la conexion a la base de datos
     */
    public void setConectionOptions(ConectionOptions conectionOptions) {
        this.conectionOptions = conectionOptions;
    }

    /**
     *
     * @return FileOptions
     * 
     * devuelve la ventana de la configuracion de ficheros de generacion de datos
     */
    public FileOptions getFileOptions() {
        return fileOptions;
    }

    /**
     *
     * @param fileOptions type: FileOptions
     * 
     * Establece la ventana de opciones de ficheros de generacionde datos
     */
    public void setFileOptions(FileOptions fileOptions) {
        this.fileOptions = fileOptions;
    }
    
//////////////////////////////////////////////////////////

    /**
     *
     * @return DatabaseList 
     * 
     * Devuelve la lista de todas las bases de datos
     * @see model.vo.DatabaseList
     */
    public DatabaseList showDatabases() {
        return logica.showDatabaseList();
    }

    /**
     *
     * @param database type: String
     * @return Database
     * 
     * devuelve el objeto database con la lista de todas las bases de datso
     * @see model.vo.Database 
     */
    public Database showTables(String database) {
        return logica.showTableList(database);
    }

    /**
     * Muestra la ventana de Opciones de conexion de Mysql
     */
    public void showConectionOptions() {
 
        conectionOptions.showMysqlConfig(cf);
        conectionOptions.setVisible(true);
    
    }

    /**
     *
     * @param table type: String
     * @return model.vo.Table
     * 
     * Devuelve la lista de colummnas de la tabla.
     */
    public Table showCools(String table) {
        return logica.showCoolList(table);
    }

    /**
     *
     * @param cool type: Cool
     * @return Boolean
     * 
     * Muestra la ventana de configuracion de la columna 
     * @see vista.ColumnOptions
     */
    public boolean setCoolConfig(Cool cool) {
        return logica.setCoolConfig(cool);
    }

    /**
     *
     * @param cool type 
     */
    public void showColumnOptions(String cool) {
        Cool cl = logica.showCoolConfig(cool);
        if (cl != null) {
            columnOptions.showCoolConfig(cl);
        }

    }

    public void saveColumnOptions() {
        columnOptions.setVisible(true);
    }
    
    public void showResults(){
        logica.showResults();
    }
    
    public void generateData(int amount) {
        logica.generateData(amount);
    }

    public void updateCf(ConfConection cf) {
        this.cf=cf;
    }

    public void showFileConf() {
        fileOptions.showFileConfig(logica.getGenerador());
        fileOptions.setVisible(true);
    }

}
