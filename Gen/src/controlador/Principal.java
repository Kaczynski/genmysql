package controlador;

import javax.swing.ImageIcon;
import model.Logica;
import model.conexion.ConfConection;

import vista.ColumnOptions;
import vista.ConectionOptions;
import vista.FileOptions;
import vista.Results;

import vista.VentanaPrincipal;

public class Principal {

    VentanaPrincipal ventanaPrincipal;
    ConectionOptions conectionOptions;
    ColumnOptions columnOptions;

    Logica logica;

    Coordinador coordinador;
    FileOptions fileOptions;
    ConfConection mysqlconfig;
    Results results;

    /**
     * @param args
     */
    public static void main(String[] args) {
        Principal miPrincipal = new Principal();
        miPrincipal.iniciar();
    }

    private void iniciar() {

        ConfConection cf = new ConfConection();
        cf.setPassword("");
        cf.setPort("3306");
        cf.setServer("localhost");
        cf.setUsername("root");

        results = new Results();
        logica = new Logica();
        coordinador = new Coordinador();
        columnOptions = new ColumnOptions();
        mysqlconfig = new ConfConection();
        fileOptions = new FileOptions();

        ventanaPrincipal = new VentanaPrincipal();
        conectionOptions = new ConectionOptions();
        results.setCoordinador(coordinador);

        ventanaPrincipal.setCoordinador(coordinador);
        conectionOptions.setCoordinador(coordinador);
        columnOptions.setCoordinador(coordinador);
        logica.setCoordinador(coordinador);
        fileOptions.setCoordinador(coordinador);

        /*Relaciones con la clase coordinador*/
        coordinador.setVentanaPrincipal(ventanaPrincipal);
        coordinador.setConectionOptions(conectionOptions);
        coordinador.setLogica(logica);
        coordinador.setColumnOptions(columnOptions);
        coordinador.setCf(cf);
        coordinador.setFileOptions(fileOptions);
        coordinador.setResults(results);
        ventanaPrincipal.setVisible(true);
        try {
            ImageIcon icon = new ImageIcon(getClass().getResource("/vista/icons/logo.png"));
            
            ventanaPrincipal.setIconImage(icon.getImage());
            results.setIconImage(icon.getImage());
            fileOptions.setIconImage(icon.getImage());
            columnOptions.setIconImage(icon.getImage());
            conectionOptions.setIconImage(icon.getImage());
        } catch (Exception e) {
            System.out.println("Error al cargar icono:: " + e.getMessage());
        }
    }

}
