/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;
import model.vo.Cool;

/**
 *
 * @author lukas
 */
public class Generador {

    private String fileNames;
    private String fileLastName;
    private String fileDomains;
    private String fileCompany;
    private String fileCountry;
    private String fileCity;
    private String fileState;
    private String fileWords;

    public Generador() {
        fileNames = "C:/Users/lukas/Desktop/ApellidosEspanoles.txt";
        fileLastName = "C:/Users/lukas/Desktop/ApellidosEspanoles.txt";
        fileDomains = "C:/Users/lukas/Desktop/domains.txt";
        fileCompany = "C:/Users/lukas/Desktop/company.txt";
        fileCountry = "C:/Users/lukas/Desktop/coutry.txt";
        fileCity = "C:/Users/lukas/Desktop/Ciudades.txt";
        fileState = "C:/Users/lukas/Desktop/communidades.txt";
        fileWords = "C:/Users/lukas/Desktop/palabras.txt";
    }

    public String generate(Cool col) throws FileNotFoundException {
    	System.out.println(col.getDatacode()+" <<-----");
        switch (col.getDatacode()) {
            case 0:

                return randLine(fileNames);

            case 1:

                return randLine(fileNames) + " " + randLine(fileLastName);

            case 2:

                return randLine(fileNames) + "_" + randLine(fileLastName) + "@" + randLine(fileDomains);

            case 3:

                return randLine(fileCompany);

            case 4:
                return randDate(col.getOpciones().get(0), col.getOpciones().get(1));
            case 5:

                return randLine(fileCountry);

            case 6:

                return randLine(fileCity);

            case 7:
                return Integer.toString(random(10, 99)) + "-" + Integer.toString(random(100, 999));
            case 8:

                return randLine(fileState);

            case 9:
                String g = "";
                for (int i = 0; i < col.getOpciones().get(0); i++) {
                    g += randLine(fileWords) + " ";
                }
                return g;
            case 10:
                return randWords(fileWords, random(col.getOpciones().get(0), col.getOpciones().get(1)));
            case 11:
                return Integer.toString(random(col.getOpciones().get(0), col.getOpciones().get(1)));
            case 12:
                String patern = "#############.";
                for (int i = 0; i < col.getOpciones().get(2); i++) {
                    patern += "#";
                }
                DecimalFormat df = new DecimalFormat(patern);
                return df.format(randomFloat(col.getOpciones().get(0), col.getOpciones().get(1))).replace(",", ".");
                
            case 13:
                return Boolean.toString(randBoolean());
            case 14:
            return "C/ "+randLine(fileNames)+" "+randLine(fileNames)+" "+Integer.toString(random(10, 99)) ;
            case 15:
            return "/"+randLine(fileNames)+"/"+randLine(fileNames)+"/" ;
               
        }

        return "";
    }
    
    
    private boolean randBoolean(){
     return Math.random() < 0.5;
    }
    private int random(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    private float randomFloat(int start, int end) {
        Random rand = new Random();
        return start + rand.nextFloat() * (end - start);
    }

    private String randDate(int start, int end) {
        GregorianCalendar gc = new GregorianCalendar();

        int year = random(start, end);

        gc.set(GregorianCalendar.YEAR, year);

        int dayOfYear = random(1, gc.getActualMaximum(GregorianCalendar.DAY_OF_YEAR));

        gc.set(GregorianCalendar.DAY_OF_YEAR, dayOfYear);

        return (gc.get(GregorianCalendar.YEAR) + "/" + (gc.get(GregorianCalendar.MONTH) + 1) + "/" + gc.get(Calendar.DAY_OF_MONTH));

    }

    private static String randLine(String file) throws FileNotFoundException {
        String result = null;
        Random rand = new Random();
        int n = 0;
        for (Scanner sc = new Scanner(new File(file)); sc.hasNext();) {
            ++n;
            String line = sc.nextLine();
            if (rand.nextInt(n) == 0) {
                result = line;
            }
        }

        return result;
    }

    private static String randWords(String file, int words) throws FileNotFoundException {
        String result = null;
        Random rand = new Random();
        int n = 0;
        int intentos=0;
        boolean ok=true;
        do{
            ok=true;
        for (Scanner sc = new Scanner(new File(file)); sc.hasNext();) {
            n++;
            String line = sc.nextLine();
            if (rand.nextInt(n) == 0) {
                result = line;
                for (int i = 1; i < words; i++) {
                    sc.hasNext();
                    try{
                    result += " " + sc.nextLine();
                    }catch(NoSuchElementException e){
                        ok= false;
                        intentos++;
                    }
                }

            }
        }
        }while(intentos<5 && !ok);
        if(intentos >5){
            System.out.println("No se puede leer del fichero no hay contenido suficiente");
            
        }
        return result;
    }

    public String getFileNames() {
        return fileNames;
    }

    public void setFileNames(String fileNames) {
        this.fileNames = fileNames;
    }

    public String getFileLastName() {
        return fileLastName;
    }

    public void setFileLastName(String fileLastName) {
        this.fileLastName = fileLastName;
    }

    public String getFileDomains() {
        return fileDomains;
    }

    public void setFileDomains(String fileDomains) {
        this.fileDomains = fileDomains;
    }

    public String getFileCompany() {
        return fileCompany;
    }

    public void setFileCompany(String fileCompany) {
        this.fileCompany = fileCompany;
    }

    public String getFileCountry() {
        return fileCountry;
    }

    public void setFileCountry(String fileCountry) {
        this.fileCountry = fileCountry;
    }

    public String getFileCity() {
        return fileCity;
    }

    public void setFileCity(String fileCity) {
        this.fileCity = fileCity;
    }

    public String getFileState() {
        return fileState;
    }

    public void setFileState(String fileState) {
        this.fileState = fileState;
    }

    public String getFileWords() {
        return fileWords;
    }

    public void setFileWords(String fileWords) {
        this.fileWords = fileWords;
    }
    
    
}
