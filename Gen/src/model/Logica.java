package model;

import javax.swing.JOptionPane;

import controlador.Coordinador;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import model.dao.CoolDao;
import model.dao.DatabaseDao;
import model.dao.DatabaseListDao;
import model.vo.Cool;
import model.vo.Database;
import model.vo.DatabaseList;
import model.vo.Table;

public class Logica {

    private Coordinador coordinador;
    private DatabaseList databaseList;
    private String databaseSelected;
    private String tableSelected;

    private Generador generador;

    public ArrayList<String> getGenerador() {
        ArrayList<String> g = new ArrayList<>();
        g.add(generador.getFileNames());
        g.add(generador.getFileLastName());
        g.add(generador.getFileDomains());
        g.add(generador.getFileCompany());
        g.add(generador.getFileCountry());
        g.add(generador.getFileCity());
        g.add(generador.getFileState());
        g.add(generador.getFileWords());
        return g;
    }

    public Logica() {

        generador = new Generador();
    }

    public void setCoordinador(Coordinador miCoordinador) {
        this.coordinador = miCoordinador;

    }

    private void loadCools(String table) {
        if (table != null && !table.contains(" ") && !table.equals("")) {
            for (Database db : databaseList.getDatabases()) {
                if (db.getName().equals(databaseSelected)) {
                    for (Table tb : db.getTables()) {
                        if (tb.getName().equals(table)) {
                            tb.setCools(new CoolDao().CoolDao(databaseSelected, table, coordinador.getCf()));
                            break;
                        }
                    }

                }
            }
        }
    }

    private void loadTable(String database) {
        if (database != null && !database.contains(" ") && !database.equals("")) {
            for (Database db : databaseList.getDatabases()) {
                if (db.getName().equals(database)) {
                    db.setTables(new DatabaseDao().DatabaseDao(database, coordinador.getCf()));
                    break;
                }
            }
        }
    }

    private void loadDatabaseList() {
        databaseList = new DatabaseListDao().getDatabaseList(coordinador.getCf());
    }

    public DatabaseList showDatabaseList() {
        if (databaseList == null) {
            loadDatabaseList();
        }
        return databaseList;
    }

    private Database databaseExists(String database) {
        for (Database db : databaseList.getDatabases()) {
            if (db.getName().equals(database)) {
                return db;
            }
        }
        return null;
    }

    private Table tableExists(String table) {
        if (databaseSelected != null) {
            Database db = databaseExists(databaseSelected);
            for (Table tb : db.getTables()) {
                if (tb.getName().equals(table)) {
                    return tb;
                }
            }
        }
        return null;
    }

    public Database showTableList(String database) {
        Database db = databaseExists(database);
        if (db != null) {
            if (db.getTables() == null || db.getTables().isEmpty()) {
                System.out.println("Tablas no cargadas");
                loadTable(database);
            }
            databaseSelected = database;
            return db;
        }
        JOptionPane.showMessageDialog(null, "No se encuentra la base de datos: " + database, "Error", JOptionPane.ERROR_MESSAGE);
        return null;
    }

    public Table showCoolList(String table) {

        Table tb = tableExists(table);
        if (tb != null) {
            if (tb.getCools() == null || tb.getCools().isEmpty()) {
                loadCools(table);
            }
            tableSelected = table;
            return tb;
        } else {
            return null;
        }
    }

    public Cool showCoolConfig(String cool) {
        if (databaseSelected != null) {
            Database db = databaseExists(databaseSelected);
            for (Table tb : db.getTables()) {
                if (tb.getName().equals(tableSelected)) {
                    for (Cool cl : tb.getCools()) {
                        if (cl.getName().equals(cool)) {
                            return cl;
                        }
                    }
                    JOptionPane.showMessageDialog(null, "No se encuentra la cool: " + cool, "Error", JOptionPane.ERROR_MESSAGE);
                    return null;
                }
            }
            JOptionPane.showMessageDialog(null, "No se encuentra la BD: " + databaseSelected, "Error", JOptionPane.ERROR_MESSAGE);
            return null;
        }
        JOptionPane.showMessageDialog(null, "No se ha selecionado la BD: " + databaseSelected, "Error", JOptionPane.ERROR_MESSAGE);
        return null;
    }

    public boolean setCoolConfig(Cool c) {
        Table tb = tableExists(tableSelected);
        if (tb != null) {
            for (int i = 0; i < tb.getCools().size(); i++) {
                if (tb.getCools().get(i).getName().equals(c.getName())) {
                    tb.getCools().set(i, c);
                    return true;
                }
            }
            JOptionPane.showMessageDialog(null, "No se ha selecionado la columna: " + c.getName(), "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        JOptionPane.showMessageDialog(null, "No se ha selecionado la tabla ", "Error", JOptionPane.ERROR_MESSAGE);
        return false;
    }

    public void showResults() {
        Table tb = tableExists(tableSelected);
        if (tb != null) {
            coordinador.getResults().initialData(tb, databaseSelected);
            coordinador.getResults().setVisible(true);
        } else {

            JOptionPane.showMessageDialog(null, "No se ha selecionado la tabla ", "Error", JOptionPane.ERROR_MESSAGE);

        }
    }

    public void generateData(int amount) {
        System.out.format("\n\n%-40s%-40s%-40s%-40s\n\n",
                "Nombre", "Code", "Type", "Data");
        Table tb = tableExists(tableSelected);
        if (tb != null) {
            String[][] data = new String[amount][tb.getCools().size()];
            for (int j = 0; j < amount; j++) {
                //System.out.print("INSERT INTO " + tableSelected + " VALUES (");
                //String result = "";
                for (int i = 0; i < tb.getCools().size(); i++) {

                    try {

                        if (tb.getCools().get(i).isAutoIncrement()) {
                            data[j][i] = "null";
                        } else if (tb.getCools().get(i).isIncremental()) {
                            data[j][i] = Integer.toString(j + 1);
                        } else {
                            data[j][i] = generador.generate(tb.getCools().get(i));
                        }

                        

                        System.out.format("%-40s%-40d%-40s%-40s\n",
                                tb.getCools().get(i).getName(),
                                tb.getCools().get(i).getDatacode(),
                                tb.getCools().get(i).getType(),
                                data[j][i]
                        );
                    } catch (FileNotFoundException ex) {
                        JOptionPane.showMessageDialog(null, ex.getCause(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
                //result = result.replaceAll(",$", "");
                //System.out.println(result + " );");

            }
            coordinador.getResults().showTableResults(data);
        } else {
            JOptionPane.showMessageDialog(null, "No se ha selecionado la tabla ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void updateConfFile(ArrayList<String> s) {
        generador.setFileNames(s.get(0));
        generador.setFileLastName(s.get(1));
        generador.setFileDomains(s.get(2));
        generador.setFileCompany(s.get(3));
        generador.setFileCountry(s.get(4));
        generador.setFileCity(s.get(5));
        generador.setFileState(s.get(6));
        generador.setFileWords(s.get(7));
    }

}
