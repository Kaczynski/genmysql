package model.conexion;

import java.sql.*;
import javax.swing.JOptionPane;

public class Conexion {
   Connection conn = null;

   public Conexion(ConfConection cf) {
       if(cf==null){
           System.out.println("No cf cargado");
       }else{
      try{

         Class.forName("com.mysql.jdbc.Driver");
         
         conn = DriverManager.getConnection("jdbc:mysql://"+cf.getServer()+":"+cf.getPort()+"/",cf.getUsername(),cf.getPassword());

         if (conn!=null){
            System.out.println("Conecci�n a base de datos OK");
         }
      }
      catch(SQLException e){
         System.out.println("tipo 1"+e);
         JOptionPane.showMessageDialog(null, "Hay un problema con los ajustes de conexion. ", "Error", JOptionPane.ERROR_MESSAGE);
      }catch(ClassNotFoundException e){
         System.out.println("tipo 2"+e);
      }catch(Exception e){
         System.out.println("Tipo 3"+e);
      }
       }
   }
   public Connection getConnection(){
      return conn;
   }

   public void desconectar(){
      conn = null;
   }
}