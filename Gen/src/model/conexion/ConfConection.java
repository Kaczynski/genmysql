/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.conexion;

/**
 *
 * @author lukas
 */
public class ConfConection {

    /**
     *
     */
    private String server;
    private  String port;
    private  String username;
    private  String password;
    

    public static void saveConfig(){
        System.out.println("model.conexion.ConfConection.saveConfig() :: No Implementado");
    }

    public  String getServer() {
        System.out.println("return server: "+server);
        return server;
    }

    public  void setServer(String server) {
        this.server = server;
    }

    public  String getPort() {
        return port;
    }

    public  void setPort(String port) {
        this.port = port;
    }

    public  String getUsername() {
        return username;
    }

    public  void setUsername(String username) {
        this.username = username;
    }

    public  String getPassword() {
        return password;
    }

    public  void setPassword(String password) {
        this.password = password;
    }
    
}
