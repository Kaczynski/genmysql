/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.conexion.Conexion;
import model.conexion.ConfConection;
import model.vo.Table;

/**
 *
 * @author lukas
 */
public class DatabaseDao {

    public LinkedList<Table> DatabaseDao(String dbname, ConfConection cf) {
        Conexion conex = new Conexion(cf);

        try {
            LinkedList<Table> tableli = new LinkedList<>();
            Statement stmt = conex.getConnection().createStatement();
            try (ResultSet res = stmt.executeQuery("SELECT TABLE_NAME AS _tables FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA =  '" + dbname+"'")) {
                while (res.next()) {
                    Table table = new Table();
                    table.setName(res.getString("_tables"));
                    tableli.add(table);
                }
            }
            conex.desconectar();
            return tableli;
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseListDao.class.getName()).log(Level.INFO, null, ex);
        }
        return null;
    }
}
