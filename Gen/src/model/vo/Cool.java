/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.vo;

import java.util.ArrayList;

/**
 *
 * @author lukas
 */
public class Cool {

    private String name;        //nombre
    private String type;        //Tipo de dato de la tabla
    private int length;         //longitud
    private boolean active;     //activo
    private boolean autoIncrement;
    private boolean incremental;
    private int datacode;       //tipo de dato
    private ArrayList<Integer> opciones;

    public boolean isIncremental() {
        return incremental;
    }

    public void setIncremental(boolean incremental) {
        this.incremental = incremental;
    }

    public Cool() {
        opciones = new ArrayList<>();
    }

    public boolean isAutoIncrement() {
        return autoIncrement;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
    }

    public ArrayList<Integer> getOpciones() {
        return opciones;
    }

    public void setOpciones(ArrayList<Integer> opciones) {
        this.opciones = opciones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getDatacode() {
        return datacode;
    }

    public void setDatacode(int datacode) {
        this.datacode = datacode;
    }

}
