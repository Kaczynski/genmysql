/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.vo;

import java.util.LinkedList;


/**
 *
 * @author lukas
 */
public class Database {
    private String name;
    private LinkedList<Table> tables;
    
    public Database(){
        tables=new LinkedList<>();
    }
    public Database(String name){
        tables=new LinkedList<>();
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<Table> getTables() {
        return tables;
    }

    public void setTables(LinkedList<Table> tables) {
        this.tables = tables;
    }
}
