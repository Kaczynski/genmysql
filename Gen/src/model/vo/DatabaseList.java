/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.vo;

import java.util.LinkedList;


/**
 *
 * @author lukas
 */
public class DatabaseList {
    private LinkedList<Database> databases;
    
    public DatabaseList(){
        databases=new LinkedList<>();
    }

    public LinkedList<Database> getDatabases() {
        return databases;
    }

    public void addDatabase(Database database) {
        this.databases.add(database);
    }
}
