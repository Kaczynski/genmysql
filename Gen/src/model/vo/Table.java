/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.vo;

import java.util.LinkedList;

/**
 *
 * @author lukas
 */
public class Table {

    private LinkedList<Cool> cools;
    private String name;

    public LinkedList<Cool> getCools() {
        return cools;
    }

    public void setCools(LinkedList<Cool> cools) {
        this.cools = cools;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Table() {
        cools = new LinkedList<>();
    }
}
