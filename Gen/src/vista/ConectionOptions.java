/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Coordinador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import model.conexion.ConfConection;
import vista.componentes.buttons.ButtonDefault;
import vista.loockAndFeell.Colours;

/**
 *
 * @author lukas
 */
public class ConectionOptions extends JFrame implements ActionListener {

    private Coordinador coordinador;
    private JTextField fielForServer;
    private JTextField fielForPort;
    private JTextField fielForUser;
    private JTextField fielForPwd;
    private ButtonDefault btnCancel;
    private ButtonDefault btnSave;

    public ConectionOptions() {
        initComponents();
    }

    private void initComponents() {

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(450, 250);
        this.setTitle("Mysql Config");

        JPanel container = new JPanel();
        container.setBackground(Colours.BACKGROUND);
        container.setLayout(null);

        fielForServer = new JTextField();
        fielForPort = new JTextField();
        fielForUser = new JTextField();
        fielForPwd = new JTextField();

        JLabel labelForServer = new JLabel("Servidor:");
        JLabel labelForPort = new JLabel("Puerto:");
        JLabel labelForUser = new JLabel("Usuario:");
        JLabel labelForPwd = new JLabel("Contraseña:");

        labelForServer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelForPort.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelForUser.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelForPwd.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        labelForServer.setBounds(10, 10, 200, 30);
        labelForPort.setBounds(10, 50, 200, 30);
        labelForUser.setBounds(10, 90, 200, 30);
        labelForPwd.setBounds(10, 130, 200, 30);

        labelForServer.setForeground(Colours.WHITE);
        labelForPort.setForeground(Colours.WHITE);
        labelForUser.setForeground(Colours.WHITE);
        labelForPwd.setForeground(Colours.WHITE);

        this.fielForServer.setBounds(220, 10, 200, 30);
        this.fielForPort.setBounds(220, 50, 200, 30);
        this.fielForUser.setBounds(220, 90, 200, 30);
        this.fielForPwd.setBounds(220, 130, 200, 30);
        container.add(labelForServer);
        container.add(labelForPort);
        container.add(labelForUser);
        container.add(labelForPwd);

        container.add(this.fielForServer);
        container.add(this.fielForPort);
        container.add(this.fielForUser);
        container.add(this.fielForPwd);

        btnCancel = new ButtonDefault(2);
        btnSave = new ButtonDefault(1);

        btnCancel.setBounds(10, 170, 200, 30);
        btnSave.setBounds(220, 170, 200, 30);
        btnCancel.setText("Cancelar");
        btnSave.setText("Aceptar");
        btnCancel.setActionCommand("close");
        btnSave.setActionCommand("save");
        btnCancel.addActionListener(this);
        btnSave.addActionListener(this);

        container.add(btnSave);
        container.add(btnCancel);
        this.getContentPane().add(container);
        //showMysqlConfig();
    }

    public void setCoordinador(Coordinador miCoordinador) {
        this.coordinador = miCoordinador;
    }

    public void showMysqlConfig(ConfConection cf) {
        System.out.println(cf.getServer());

        fielForServer.setText(cf.getServer());
        fielForPort.setText(cf.getPort());
        fielForUser.setText(cf.getUsername());
        fielForPwd.setText(cf.getPassword());

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnSave) {
            save();
            this.dispose();
        }
        if (e.getSource() == btnCancel) {
            this.dispose();
        }
    }

    private void save() {
        ConfConection cf = new ConfConection();
        cf.setPassword(this.fielForPwd.getText());

        cf.setPort(fielForPort.getText());
        cf.setServer(fielForServer.getText());
        cf.setUsername(fielForUser.getText());
        coordinador.updateCf(cf);
    }
}
