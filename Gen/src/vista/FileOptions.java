/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Coordinador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.conexion.ConfConection;
import vista.componentes.buttons.ButtonDefault;
import vista.loockAndFeell.Colours;

/**
 *
 * @author lukas
 */
public class FileOptions extends JFrame implements ActionListener {

    private Coordinador coordinador;

    private ButtonDefault btnCancel;
    private ButtonDefault btnSave;

    private ArrayList<JLabel> labels;
    private ArrayList<JTextField> text;
    private ArrayList<ButtonDefault> button;
    private final String[] components;

    public FileOptions() {
        components = new String[]{
            "Nombres",
            "Apellidos",
            "Dominios",
            "Companias",
            "Paises",
            "Ciuddades",
            "Comunidades",
            "Palabras"};

        labels = new ArrayList<>();
        text = new ArrayList<>();
        button = new ArrayList<>();
        initComponents();
    }

    private void initComponents() {

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        this.setTitle("Source files.");

        JPanel container = new JPanel();
        container.setBackground(Colours.BACKGROUND);
        container.setLayout(null);

        for (int i = 0; i < components.length; i++) {

            JLabel label = new JLabel(components[i] + ":");
            label.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
            label.setBounds(10, 10 + (40 * i), 100, 30);
            label.setForeground(Colours.WHITE);
            labels.add(label);
            container.add(label);

            JTextField textf = new JTextField("Null");
            textf.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
            textf.setBounds(120, 10 + (40 * i), 200, 30);
            textf.setForeground(Colours.BACKGROUND);
            text.add(textf);
            container.add(textf);

            ButtonDefault btn = new ButtonDefault(0, 4);
            btn.setBounds(330, 10 + (40 * i), 100, 30);
            btn.setText("File");
            btn.setActionCommand("showFileChoiser");
            btn.setDatatype(i);
            btn.addActionListener(this);
            container.add(btn);
            button.add(btn);
        }
        btnCancel = new ButtonDefault(2);
        btnSave = new ButtonDefault(1);

        btnCancel.setBounds(10, 10 + (components.length * 40), 200, 30);
        btnSave.setBounds(220, 10 + (components.length * 40), 200, 30);
        btnCancel.setText("Cancelar");
        btnSave.setText("Aceptar");
        btnCancel.setActionCommand("close");
        btnSave.setActionCommand("save");
        btnCancel.addActionListener(this);
        btnSave.addActionListener(this);

        container.add(btnSave);
        container.add(btnCancel);
        this.getContentPane().add(container);
        this.setSize(450, 90 + (components.length * 40));
    }

    public void setCoordinador(Coordinador miCoordinador) {
        this.coordinador = miCoordinador;
    }

    public void showFileConfig(ArrayList<String> cf) {
        for(int i =0; i<cf.size();i++){
            text.get(i).setText(cf.get(i));
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnSave) {
            save();
        }
        if (e.getSource() == btnCancel) {
            this.dispose();
        }
        if (e.getActionCommand().equals("showFileChoiser")) {
            int index = ((ButtonDefault) e.getSource()).getDatatype();
            String g = selectFile(index);
            if (g != null) {
                text.get(index).setText(g);
            }
        }

    }

    private String selectFile(int e) {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Seleccione " + components[e]);
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "TXT");
        chooser.setFileFilter(filter);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile().toString();
        } else {
            return null;
        }

    }

    private void save() {
        ArrayList<String> s = new ArrayList<>();
        for (JTextField f : text) {
            s.add(f.getText());
        }
        coordinador.getLogica().updateConfFile(s);
        this.dispose();
    }
}
