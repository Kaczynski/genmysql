/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Coordinador;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import model.vo.Table;
import vista.componentes.buttons.ButtonDefault;
import vista.loockAndFeell.Colours;

/**
 *
 * @author lukas
 */
public class Results extends JFrame implements ActionListener {

    private Coordinador coordinador;
    private Table tb;
    private ButtonDefault generar;
    private ButtonDefault clear;
    private ButtonDefault generarText;
    private JLabel labelForAmount;
    private JLabel labelForDB;
    private JLabel labelForTB;
    private JLabel labelForState;
    private JSplitPane jSplitPane1;
    private JPanel contenedor;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JTable tabla;
    private JTextArea text;
    private JTextField amount;
    private boolean editable;
    private boolean guardado;

    /**
     * constructor de la ventana de seleccion de cantidad de resultados a
     * generar con su posteriror visalizacion
     */
    public Results() {
        init();
    }

    /**
     *
     * @param miCoordinador type: Coordinador
     */
    public void setCoordinador(Coordinador miCoordinador) {
        this.coordinador = miCoordinador;
    }

    /**
     * Muestra los datos generados en la tabla de contenido
     *
     * @param data String[][]
     */
    public void showTableResults(String[][] data) {
        int response;
        if (!guardado) {
            String[] options = new String[]{"Borrar datos existentes y seguir", "Guardar los datos y seguir", "Añadir a datos existentes", "Cancel"};
            response = JOptionPane.showOptionDialog(null, "Tiene datos sin guardar.\n Quiere desea hacer?", "Ojo!",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, options, options[0]);
        } else {
            response = 2;
        }
        guardado = false;
        switch (response) {
            case 0:
                editable = false;
                text.setText("");
                for (int i = ((DefaultTableModel) tabla.getModel()).getRowCount() - 1; i >= 0; i--) {
                    ((DefaultTableModel) tabla.getModel()).removeRow(i);
                }
                for (String[] data1 : data) {
                    ((DefaultTableModel) tabla.getModel()).addRow(data1);
                }
                editable = true;
                break;
            case 1:

                break;
            case 2:
                editable = false;
                for (String[] data1 : data) {
                    ((DefaultTableModel) tabla.getModel()).addRow((Object[]) data1);
                    
                }
                editable = true;
                break;

        }

    }

    /**
     * Muestra los resultados que estan almacenados en la tabla de contenido en
     * formato mysql
     */
    public void showTextResults() {
        text.setText("");
        String cabecera = "INSERT INTO " + tb.getName() + " (";
        for (int i = 0; i < tabla.getModel().getColumnCount(); i++) {
            cabecera += " " + tabla.getModel().getColumnName(i).split(" ")[0] + ",";
        }
        cabecera = cabecera.replaceAll(",$", "") + " ) VALUES ( ";

        for (int j = 0; j < tabla.getModel().getRowCount(); j++) {
            String datos = "";
            for (int i = 0; i < tabla.getModel().getColumnCount(); i++) {

                if (tb.getCools().get(i).isAutoIncrement() || tb.getCools().get(i).isIncremental()) {
                    datos += tabla.getModel().getValueAt(j, i) + ", ";

                } else {
                    switch (tb.getCools().get(i).getDatacode()) {
                        case 11:
                            datos += tabla.getModel().getValueAt(j, i) + ", ";
                            break;
                        case 12:
                            datos += tabla.getModel().getValueAt(j, i) + ", ";
                            break;
                        case 13:
                            datos += tabla.getModel().getValueAt(j, i) + ", ";
                            break;
                        default:
                            datos += "\"" + tabla.getModel().getValueAt(j, i) + "\",";

                    }
                }

            }
            datos = datos.replaceAll(", $", "");
            text.setText(text.getText() + cabecera + datos + ");\n");

        }

    }

    private void init() {
        guardado = true;

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        contenedor = new JPanel();
        labelForAmount = new JLabel();
        labelForDB = new JLabel();
        labelForTB = new JLabel();
        labelForState = new JLabel();
        amount = new JTextField();
        generar = new ButtonDefault(1);
        generarText = new ButtonDefault(1);
        clear = new ButtonDefault(2);
        jScrollPane1 = new JScrollPane();
        text = new JTextArea();
        jScrollPane2 = new JScrollPane();
        tabla = new JTable();
        jSplitPane1 = new javax.swing.JSplitPane();

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setDividerLocation(200);

        getContentPane().setBackground(Colours.BACKGROUND);
        getContentPane().setLayout(new java.awt.BorderLayout(0, 20));

        contenedor.setBackground(Colours.BACKGROUND);
        contenedor.setLayout(new java.awt.GridLayout(2, 4, 0, 10));

        labelForState.setText("Operando sobre:");
        labelForState.setForeground(Colours.WHITE);
        contenedor.add(labelForState);

        labelForDB.setText("Database");
        labelForDB.setForeground(Colours.WHITE);
        contenedor.add(labelForDB);

        labelForTB.setText("Table name");
        labelForTB.setForeground(Colours.WHITE);
        contenedor.add(labelForTB);

        clear.setText("Guardar");
        clear.addActionListener(this);
        contenedor.add(clear);

        labelForAmount.setText("Cantidad a generar:");
        labelForAmount.setForeground(Colours.WHITE);
        contenedor.add(labelForAmount);

        amount.setText("5");
        amount.setForeground(Colours.WHITE);
        css(amount);
        contenedor.add(amount);

        tabla.setColumnSelectionAllowed(true);
        tabla.setGridColor(new java.awt.Color(51, 51, 51));
        tabla.getTableHeader().setBackground(Colours.DEFAULT);
        tabla.getTableHeader().setForeground(Colours.WHITE);
        tabla.setSelectionBackground(Colours.SUCCESS);
        tabla.setSelectionForeground(Colours.WHITE);
        /*tabla.setForeground(Colours.WHITE);
        tabla.setGridColor(Colours.WHITE_GREY);
        
        tabla.setBackground(Colours.BACKGROUND);
         */

        generar.setText("Generar Datos");
        generar.addActionListener(this);
        contenedor.add(generar);

        generarText.setText("Generar Mysql");
        generarText.addActionListener(this);
        contenedor.add(generarText);

        getContentPane().add(contenedor, java.awt.BorderLayout.PAGE_START);

        text.setColumns(20);
        text.setRows(5);
        jScrollPane1.setViewportView(text);

        //getContentPane().add(jScrollPane1, java.awt.BorderLayout.PAGE_END);
        jScrollPane2.setOpaque(false);
        jScrollPane2.setViewportView(tabla);
        jScrollPane2.setBackground(Colours.BACKGROUND);
        jScrollPane2.getViewport().setBackground(Colours.BACKGROUND);

        jSplitPane1.setLeftComponent(jScrollPane2);

        getContentPane().setBackground(Colours.BACKGROUND);
        jSplitPane1.setRightComponent(jScrollPane1);

        getContentPane().add(jSplitPane1, java.awt.BorderLayout.CENTER);

        pack();
    }

    private void css(JTextField int1) {
        int1.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createMatteBorder(0, 0, 2, 0, Color.yellow),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        int1.setBackground(Colours.BACKGROUND_LIGTH);
        int1.setForeground(Colours.WHITE);
        int1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        int1.setCaretColor(Colours.WHITE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == generar) {
            try {
                coordinador.generateData(Integer.parseInt(amount.getText()));
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "En el campo cantidad solo se admiten enteros", "Error", JOptionPane.ERROR_MESSAGE);

            }
        }
        if (e.getSource() == clear) {

        }
        if (e.getSource() == generarText) {
            System.out.println("Entra");
            showTextResults();
        }

    }

    /**
     * Crea la cabecera de la tabla de resultados.
     *
     * @param tb type: Table
     * @param databaseSelected type: String
     */
    public void initialData(Table tb, String databaseSelected) {
        editable = false;
        labelForTB.setText(tb.getName());
        labelForDB.setText(databaseSelected);
        this.tb = tb;
        String[][] empty = new String[1][tb.getCools().size()];
        String[] header = new String[tb.getCools().size()];
        for (int i = 0; i < tb.getCools().size(); i++) {
            header[i] = tb.getCools().get(i).getName() + " {"
                    + tb.getCools().get(i).getType() + ", "
                    + Integer.toString(tb.getCools().get(i).getLength()) + "}";
        }

        DefaultTableModel model;
        model = new DefaultTableModel(header, 0) {
            @Override
            public boolean isCellEditable(int row, int col) {
                return true;
            }
        };
        model.addTableModelListener((TableModelEvent e) -> {
            if (editable) {
                System.out.println("Loool xD");
                System.out.println("Col:" + e.getColumn());
                System.out.println("Row:" + e.getFirstRow());
                System.out.println(tabla.getModel().getValueAt(e.getFirstRow(), e.getColumn()));
                guardado = false;
            }
        });
        tabla.setModel(model);
        editable = true;
    }

}
