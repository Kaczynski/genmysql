package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import controlador.Coordinador;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;
import javax.swing.OverlayLayout;
import model.vo.Database;
import model.vo.DatabaseList;
import model.vo.Table;
import vista.componentes.buttons.ButtonDefault;
import vista.componentes.panels.ComponentPanel;
import vista.componentes.panels.PanelGrey;
import vista.componentes.panels.ScrollPanel;

public class VentanaPrincipal extends JFrame implements ActionListener {

    private Coordinador miCoordinador; //objeto miCoordinador que permite la relacion entre esta clase y la clase coordinador

    private ButtonDefault buttonOptios;
    private ButtonDefault buttonGenerate;
    private ButtonDefault buttonGenerateOptions;
    private ButtonDefault buttonShowDatabases;
    private PanelGrey container;
    private ScrollPanel databaseContainer;
    private ScrollPanel tablesContainer;
    private ScrollPanel coolContainer;

    public VentanaPrincipal() {
        initComponents();
    }

    private void initComponents() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setMinimumSize(new Dimension(650, 500));
        container = new PanelGrey();
        this.addWindowStateListener((WindowEvent e) -> {
            componentsResize();
        });

        container.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                componentsResize();
            }

        });

        databaseContainer = new ScrollPanel();
        databaseContainer.setBounds(10, 30, 210, 300);
        databaseContainer.getVerticalScrollBar().setUI(new MyScrollBarUI());
        databaseContainer.setHorizontalScrollBar(null);
        container.add(databaseContainer);

        tablesContainer = new ScrollPanel();
        tablesContainer.setBounds(240, 30, 210, 300);
        tablesContainer.getVerticalScrollBar().setUI(new MyScrollBarUI());
        tablesContainer.setHorizontalScrollBar(null);
        container.add(tablesContainer);

        coolContainer = new ScrollPanel();
        coolContainer.setBounds(480, 30, 210, 300);
        coolContainer.getVerticalScrollBar().setUI(new MyScrollBarUI());
        coolContainer.setHorizontalScrollBar(null);
        container.add(coolContainer);

        buttonOptios = new ButtonDefault(0, 4);
        buttonOptios.setText("Ajustes");
        buttonOptios.setBounds(0, 0, 100, 25);
        buttonOptios.addActionListener(this);
        container.add(buttonOptios);
        
        buttonShowDatabases = new ButtonDefault(1, 3);
        buttonShowDatabases.setText("Conectar");
        buttonShowDatabases.setBounds(100, 0, 150, 25);
        buttonShowDatabases.addActionListener(this);
        container.add(buttonShowDatabases);
        getContentPane().setLayout(new OverlayLayout(getContentPane()));
        add(container);
        
        buttonGenerate = new ButtonDefault(1, 4);
        buttonGenerate.setText("Generar");
        buttonGenerate.setBounds(360, 0, 100, 25);
        buttonGenerate.addActionListener(this);
        container.add(buttonGenerate);
        
        buttonGenerateOptions = new ButtonDefault(0, 4);
        buttonGenerateOptions.setText("GenConf");
        buttonGenerateOptions.setBounds(260, 0, 100, 25);
        buttonGenerateOptions.addActionListener(this);
        container.add(buttonGenerateOptions);
    }

    public void setCoordinador(Coordinador miCoordinador) {
        this.miCoordinador = miCoordinador;
    }

    private void showDatabases(DatabaseList databaseList) {
        databaseContainer.clear();
        tablesContainer.clear();
        coolContainer.clear();
        for (int i = 0; i < databaseList.getDatabases().size(); i++) {
            //System.out.println("DB:" + databaseList.getDatabases().get(i).getName());
            ComponentPanel db = new ComponentPanel("conection");
            db.setBounds(10, (db.getBounds().height + 10) * i + 10, db.getBounds().width, db.getBounds().height);

            ButtonDefault btd = new ButtonDefault(0);
            btd.setText(databaseList.getDatabases().get(i).getName());
            btd.addActionListener(this);
            btd.setDatatype(0);
            btd.setActionCommand("databaseSelect");
            btd.setNames(databaseList.getDatabases().get(i).getName());
            db.addButton(btd);
            databaseContainer.addDb(db);

        }
        databaseContainer.paint();
        tablesContainer.paint();
        coolContainer.paint();
        repaint();
        validate();
    }

    private void showTables(Database database) {
        tablesContainer.clear();
        coolContainer.clear();
        for (int i = 0; i < database.getTables().size(); i++) {
            ComponentPanel db = new ComponentPanel("server");
            db.setBounds(10, (db.getBounds().height + 10) * i + 10, db.getBounds().width, db.getBounds().height);

            ButtonDefault btd = new ButtonDefault(0);
            btd.setText(database.getTables().get(i).getName());
            btd.addActionListener(this);
            btd.setDatatype(0);
            btd.setActionCommand("tableSelect");
            btd.setNames(database.getTables().get(i).getName());
            db.addButton(btd);
            tablesContainer.addDb(db);

        }
        tablesContainer.paint();
        coolContainer.paint();
        repaint();
        validate();
    }

    private void showCools(Table table) {
        coolContainer.clear();
        for (int i = 0; i < table.getCools().size(); i++) {
            ComponentPanel db = new ComponentPanel("cool");
            db.setBounds(10, (db.getBounds().height + 10) * i + 10, db.getBounds().width, db.getBounds().height);

            ButtonDefault btd = new ButtonDefault(0);
            btd.setText(table.getCools().get(i).getName());
            btd.addActionListener(this);
            btd.setDatatype(0);
            btd.setActionCommand("coolSelect");
            btd.setNames(table.getCools().get(i).getName());

            ButtonDefault btc = new ButtonDefault(2);
            btc.setActionCommand("coolConfig");
            btc.addActionListener(this);
            btc.setNames(table.getCools().get(i).getName());
            
            db.setConfButton(btc);
            db.addButton(btd);
            coolContainer.addDb(db);

        }
        coolContainer.paint();
        repaint();
        validate();
    }

    private void componentsResize() {
        int aux = (getWidth() / 3 - 10);
        if (aux < 260) {
            aux = 260;
        }
        coolContainer.setBounds(aux * 2 + 10, 30, aux, getHeight() - 80);
        coolContainer.repaint();

        tablesContainer.setBounds(aux + 10, 30, aux, getHeight() - 80);
        tablesContainer.repaint();

        databaseContainer.setBounds(10, 30, aux, getHeight() - 80);
        databaseContainer.repaint();
        container.repaint();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buttonOptios) {
            miCoordinador.showConectionOptions();
        }
        
        if(e.getSource() == buttonGenerateOptions){
            miCoordinador.showFileConf();
        }
        
        if (e.getSource() == buttonGenerate) {
            miCoordinador.showResults();
        }
        
        if (e.getSource() == buttonShowDatabases) {
            DatabaseList databaseList = miCoordinador.showDatabases();
            if (databaseList != null) {
                showDatabases(databaseList);
            } else {
                JOptionPane.showMessageDialog(null, "No se ha encontrado ninguna base de datos!", "Advertencia", JOptionPane.WARNING_MESSAGE);
            }
        }
        if (e.getActionCommand().equals("databaseSelect")) {
            Database db = miCoordinador.showTables(((ButtonDefault) e.getSource()).getNames() + "");
            if (db == null) {
                JOptionPane.showMessageDialog(null, "No se ha encontrado ninguna Tabla!", "Advertencia", JOptionPane.WARNING_MESSAGE);
            } else {
                databaseContainer.clearChildStyle();
                ((ComponentPanel) ((ButtonDefault) e.getSource()).getParent()).setActive();
                showTables(db);
            }
        }
        if (e.getActionCommand().equals("tableSelect")) {
            Table table = miCoordinador.showCools(((ButtonDefault) e.getSource()).getNames() + "");
            if (table == null) {
                System.out.println("Error");
            } else {
                showCools(table);
                tablesContainer.clearChildStyle();
                ((ComponentPanel) ((ButtonDefault) e.getSource()).getParent()).setActive();
            }
        }

        if (e.getActionCommand().equals("coolConfig")) {
            miCoordinador.showColumnOptions(((ButtonDefault) e.getSource()).getNames() + "");
        }

    }
}
