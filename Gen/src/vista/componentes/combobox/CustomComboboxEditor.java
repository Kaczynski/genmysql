/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.componentes.combobox;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import vista.loockAndFeell.Colours;

/**
 *
 * @author lukas
 */

public class CustomComboboxEditor extends BasicComboBoxEditor {

    private JPanel panel;
    private JLabel labelItem;
    private String selectedValue;

    public CustomComboboxEditor() {
        init();
    }

    private void init() {
        panel = new JPanel();
        labelItem = new JLabel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.insets = new Insets(2, 5, 2, 2);

        labelItem.setOpaque(false);
        labelItem.setHorizontalAlignment(JLabel.LEFT);
        labelItem.setForeground(Color.WHITE);

        panel.add(labelItem, constraints);
        panel.setBackground(Colours.BACKGROUND_LIGTH);
    }

    @Override
    public Component getEditorComponent() {
        return this.panel;
    }

    @Override
    public Object getItem() {
        return this.selectedValue;
    }

    @Override
    public void setItem(Object item) {
        if (item == null) {
            return;
        }
        try{
        Object[] objeto = (Object[]) item;
        
        selectedValue = (String) objeto[0];
        labelItem.setText(selectedValue);
        
        }catch(Exception e){
            
        }
    }
}
