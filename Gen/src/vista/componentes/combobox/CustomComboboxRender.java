/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.componentes.combobox;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import vista.loockAndFeell.Colours;

/**
 *
 * @author lukas
 */
public class CustomComboboxRender extends JPanel implements ListCellRenderer {

    private JLabel labelItem;

    public CustomComboboxRender() {
        init();
    }

    private void init() {
        labelItem = new JLabel();
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 1.0;
        constraints.insets = new Insets(2, 2, 2, 2);

        labelItem.setOpaque(true);
        labelItem.setHorizontalAlignment(JLabel.LEFT);

        add(labelItem, constraints);
        setBackground(Colours.BACKGROUND);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected, boolean cellHasFocus) {
        Object[] item = (Object[]) value;


        labelItem.setText((String) item[0]);
        if (isSelected) {
            labelItem.setBackground(Colours.BACKGROUND_LIGTH);
            labelItem.setForeground(Colours.WHITE);
        } else {
            labelItem.setForeground(Colours.WHITE
            );
            labelItem.setBackground(Colours.BACKGROUND);
        }

        return this;
    }

}
