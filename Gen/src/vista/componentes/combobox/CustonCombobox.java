/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.componentes.combobox;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author lukas
 */
public class CustonCombobox extends JComboBox {

    private final DefaultComboBoxModel model;

    public CustonCombobox() {
        model = new DefaultComboBoxModel();
        setModel(model);
        setRenderer(new CustomComboboxRender());
        setEditor(new CustomComboboxEditor());
    }

    /**
     * Add an array items to this combo box. Each item is an array of two String
     * elements: - first element is country name. - second element is int
     *
     * @param items
     */
    public void addItems(Object[][] items) {
        for (Object anItem : items) {
            model.addElement(anItem);
        }
    }

    public int getSelectedType(int e) {
        //return (int)((Object[])model.getSelectedItem())[0];
        return (int) ((Object[]) model.getElementAt(e))[2];

    }
}
