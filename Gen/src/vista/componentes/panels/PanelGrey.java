/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.componentes.panels;

import java.awt.Color;
import javax.swing.JPanel;

/**
 *
 * @author lukas
 */
public class PanelGrey extends JPanel{
    private final Color[] COLORS= {new Color(50, 77, 92), new Color(60, 154, 204),new Color(204, 60, 17)};
    public PanelGrey() {
        setStyle();
    }
    private void setStyle(){
        this.setBackground(new Color(50, 77, 92));
        setLayout(null);
    }
    
    
}
