/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.loockAndFeell;

import java.awt.Color;

/**
 *
 * @author lukas
 */
public class Colours {
    public static final Color BACKGROUND= new Color(50, 77, 92);
    public static final Color BACKGROUND_LIGTH= new Color(26, 52, 66);
    public static final Color BACKGROUND_DARK= new Color(26, 52, 66);
    public static final Color SUCCESS= new Color(70, 178, 157);
    public static final Color SUCCESS_LIGTH= new Color(0, 0, 0);
    public static final Color SUCCESS_DATK= new Color(0, 0, 0);
    public static final Color DEFAULT= new Color(60, 154, 204); 
    public static final Color DANGER= new Color(204, 60, 17);
    public static final Color WHITE= new Color(255, 255, 255);
    public static final Color WHITE_GREY= new Color(240, 240, 240);
    
    
    /**
     * 0    foreground
     * 1    foregroundSelected
     * 2    
     */
    public static final Color[] CUSTOM_JCOMBOBOX= {new Color(255, 255, 255),new Color(255, 255, 255)};
    
}
