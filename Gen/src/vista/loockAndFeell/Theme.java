/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.loockAndFeell;

import java.io.File;
import java.net.URL;
import javax.swing.UIManager;
import javax.swing.plaf.synth.SynthLookAndFeel;

/**
 *
 * @author lukas
 */
public class Theme {
    private static final String synthFile = "/gui/loockAndFeell/ThemeDark.xml";
    public static void initLookAndFeel() {
       // String lookAndFeel = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
       SynthLookAndFeel lookAndFeel = new SynthLookAndFeel();
        
  
            try {
                lookAndFeel.load(new File("src/gui/loockAndFeell/ThemeDark.xml").toURI().toURL());
                UIManager.setLookAndFeel(lookAndFeel);
            } 
             
            catch (Exception e) {
                System.err.println("Couldn't get specified look and feel ("
                                   + lookAndFeel
                                   + "), for some reason.");
                System.err.println("Using the default look and feel.");
                e.printStackTrace();
            }
         
    }
}
