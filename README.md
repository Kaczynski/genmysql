GenMysql beta 
========

Aplicación java orientada a la población de bases de datos Mysql.

Información de uso:
====================

> Configuración de la conexión al servidor Mysql:
> -----------------------------------------------
>> Botón en la parte superior izquierda "Ajustes"


> Configuración de los ficheros fuentes de datos:
> ------------------------------------------------
>> Botón en la parte superior izquierda "GenConf".
>>> !Importante: las líneas del archivo deben de estar en formato correcto o la salida será null. Recomendable utilizar un editor como notepad++ o Sublime para su modificación o creación.


> Modo de uso:
> -------------


>> Hacemos clic en conectar, nos aparecerá una lista de bases de datos que tenemos en nuestro servidor. Seleccionamos la base y nos aparecerán las tablas de esa base de datos al seleccionarla nos aparecerán las columnas que tiene.
>>
>> Para configurar el tipo de dato que tiene cada columna aremos clic en el botón rojo con la rueda dentada que nos mostrara los ajustes de la columna. Es importante mencionar que hay que pulsar el botón aceptar para guardar la configuración de la columna.
>>
>> Para generar los datos hacemos clic en el botón generar en la parte superior izquierda de la ventana principal la cual nos abrirá nueva ventana. En esta ventana en el campo Cantidad a generar ponderemos la cantidad de los registros a generar y hacemos clic en "Generar datos" para generar los datos.
Los datos generados se mostraran en la tabla en la cual los podremos modificar a gusto si hiciese falta.
>>
>> Para obtener el Mysql de inserción aremos clic en "Generar Mysql" que nos generar a el Mysql en la parte inferior de la ventana.

Bugs
====

> +Al no encontrar un fichero salen multiples alerts vacios